from . import enums


tl_strings = {
    # =^_^=
    "en-US": {
        "CardType.POKEMON": "Pokémon",
        "CardType.TRAINER": "Trainer",
        "CardType.ENERGY": "Energy",
        "CardSubType.ITEM": "Item",
        "CardSubType.SUPPORTER": "Supporter",
        "CardSubType.STADIUM": "Stadium",
        "CardSubType.TOOL": "Pokémon Tool",
        "CardSubType.BASIC": "Basic Energy",
        "CardSubType.SPECIAL": "Special Energy",
        # "Rarity.PROMO": "FIXME",
        "Rarity.COMMON": "Common",
        "Rarity.UNCOMMON": "Uncommon",
        "Rarity.RARE": "Rare",
        "Rarity.DOUBLE_RARE": "Double Rare",
        "Rarity.ILLUSTRATION_RARE": "Illustration Rare",
        "Rarity.ULTRA_RARE": "Ultra Rare",
        "Rarity.SPECIAL_ILLUSTRATION_RARE": "Special Illustration Rare",
        "Rarity.HYPER_RARE": "Hyper Rare",
        "Tag.TERA": "Tera",
        "Tag.ANCIENT": "Ancient",
        "Tag.FUTURE": "Future",
        "PokemonStage.BASIC": "Basic",
        "PokemonStage.STAGE1": "Stage 1",
        "PokemonStage.STAGE2": "Stage 2",
        "Color.GRASS": "Grass",
        "Color.FIRE": "Fire",
        "Color.WATER": "Water",
        "Color.LIGHTNING": "Lightning",
        "Color.PSYCHIC": "Psychic",
        "Color.FIGHTING": "Fighting",
        "Color.DARKNESS": "Darkness",
        "Color.METAL": "Metal",
        "Color.FAIRY": "Fairy",
        "Color.DRAGON": "Dragon",
        "Color.COLORLESS": "Colorless",
        # Misc
        "TextKind.EFFECT": "Effect",
        "TextKind.ABILITY": "Ability",
        "TextKind.ATTACK": "Attack",
        "evolves_from": "Evolves from {0}",
        "evolves_into": "Evolves into {0}",
        "artist_credit": "Illus. {0}",
        "hp": "HP",
        "weakness": "weakness",
        "resistance": "resistance",
        "retreat": "retreat",
        ###
        "sv_tera_marker": "Tera",
        "sv_tera_text": (
            "As long as this Pokémon is on your Bench, prevent all damage done to this "
            "Pokémon by attacks (both yours and your opponent’s)."
        ),
        "sv_ex_rule_title": "Pokémon ex rule",
        "sv_ex_rule_text": (
            "When your Pokémon ex " "is Knocked Out, your opponent takes 2 Prize cards."
        ),
        "swsh_v_rule_title": "V rule",
        "swsh_v_rule_text": (
            "When your Pokémon V is Knocked Out, " "your opponent takes 2 Prize cards."
        ),
        "swsh_vmax_rule_title": "VMAX rule",
        "swsh_vmax_rule_text": (
            "When your Pokémon VMAX "
            "is Knocked Out, your opponent takes 3 Prize cards."
        ),
        "sv_supporter_text": (
            "You may play only 1 Supporter card during " "your turn."
        ),
        "sv_stadium_text": (
            "You may play only 1 Stadium card during your turn. Put it next to "
            "the Active Spot, and discard it if another Stadium comes into play. "
            "A Stadium with the same name can’t be played."
        ),
        "sv_item_text": ("You may play any number of Item cards during " "your turn."),
        "sv_tool_text": (
            "You may attach any number of Pokémon Tools to your "
            "Pokémon during your turn. You may attach only 1 "
            "Pokémon Tool to each Pokémon, and it stays attached."
        ),
    },
    # Proofread by GrollenKette951
    "de-DE": {
        "CardType.POKEMON": "Pokémon",
        "CardType.TRAINER": "Trainer",
        "CardType.ENERGY": "Energie",
        "CardSubType.ITEM": "Item",
        "CardSubType.SUPPORTER": "Unterstützer",
        "CardSubType.STADIUM": "Stadion",
        "CardSubType.TOOL": "Pokémon-Ausrüstung",
        "CardSubType.BASIC": "Basis-Energie",
        "CardSubType.SPECIAL": "Spezial-Energie",
        # "Rarity.PROMO": "FIXME",
        "Rarity.COMMON": "häufig",
        "Rarity.UNCOMMON": "nicht so häufig",
        "Rarity.RARE": "selten",
        "Rarity.DOUBLE_RARE": "doppelselten",
        "Rarity.ILLUSTRATION_RARE": "selten, Illustration",
        "Rarity.ULTRA_RARE": "ultraselten",
        "Rarity.SPECIAL_ILLUSTRATION_RARE": "selten, besondere Illustration",
        "Rarity.HYPER_RARE": "hyperselten",
        "Tag.TERA": "Terakristall",
        "Tag.ANCIENT": "Vergangenheit",
        "Tag.FUTURE": "Zukunft",
        "PokemonStage.BASIC": "Basis",
        "PokemonStage.STAGE1": "Phase 1",
        "PokemonStage.STAGE2": "Phase 2",
        "Color.GRASS": "Pflanze",
        "Color.FIRE": "Feuer",
        "Color.WATER": "Wasser",
        "Color.LIGHTNING": "Elektro",
        "Color.PSYCHIC": "Psycho",
        "Color.FIGHTING": "Kampf",
        "Color.DARKNESS": "Finsternis",
        "Color.METAL": "Metall",
        "Color.FAIRY": "Fee",
        "Color.DRAGON": "Drache",
        "Color.COLORLESS": "Farblos",
        "TextKind.EFFECT": "Effekt",
        "TextKind.ABILITY": "Fähigkeit",
        "TextKind.ATTACK": "Attacke",
        "evolves_from": "Entwickelt sich aus {0}",
        "evolves_into": "Entwickelt sich zu {0}",
        "artist_credit": "Illustr. {0}",
        "hp": "KP",
        "weakness": "Schwäche",
        "resistance": "Resistenz",
        "retreat": "Rückzug",
        "sv_tera_marker": "Terakristall",
        "sv_tera_text": (
            "Solange sich dieses Pokémon auf deiner Bank befindet, verhindere allen Schaden, "
            "der diesem Pokémon durch Attacken (deine und die deines Gegners) zugefügt wird."
        ),
        "sv_ex_rule_title": "Pokémon-ex-Regel",
        "sv_ex_rule_text": (
            "Wenn dein Pokémon-ex " "kampfunfähig ist, nimmt dein Gegner 2 Preiskarten."
        ),
        "sv_supporter_text": (
            "Du kannst während deines Zuges nur " " 1 Unterstützerkarte spielen."
        ),
        "sv_stadium_text": (
            "Du kannst während deines Zuges nur 1 Stadionkarte spielen. Lege "
            "sie neben die Aktive Position, und lege sie auf den Ablagestapel, "
            "wenn eine andere Stadionkarte ins Spiel gebracht wird. Eine "
            "Stadionkarte mit demselben Namen kann nicht gespielt werden."
        ),
        "sv_item_text": (
            "Du kannst während deines Zuges beliebig " "viele Itemkarten spielen."
        ),
        "sv_tool_text": (
            "Du kannst während deines Zuges beliebig viele Pokémon-Austrüstungen "
            "an deine Pokémon anlegen. Du kannst an jedes Pokémon nur "
            "1 Pokémon-Ausrüstung anlegen, und sie bleibt angelegt."
        ),
    },
    # Proofread by Zarmakuizz
    "fr-FR": {
        "CardType.POKEMON": "Pokémon",
        "CardType.TRAINER": "Dresseur",
        "CardType.ENERGY": "Énergie",
        "CardSubType.ITEM": "Objet",
        "CardSubType.SUPPORTER": "Supporter",
        "CardSubType.STADIUM": "Stade",
        "CardSubType.TOOL": "Outil Pokémon",
        "CardSubType.BASIC": "Énergie de base",
        "CardSubType.SPECIAL": "Énergie spéciale",
        # "Rarity.PROMO": "FIXME",
        "Rarity.COMMON": "commune",
        "Rarity.UNCOMMON": "peu commune",
        "Rarity.RARE": "rare",
        "Rarity.DOUBLE_RARE": "double rare",
        "Rarity.ILLUSTRATION_RARE": "illustration rare",
        "Rarity.ULTRA_RARE": "ultra rare",
        "Rarity.SPECIAL_ILLUSTRATION_RARE": "illustration spéciale rare",
        "Rarity.HYPER_RARE": "hyper rare",
        "Tag.TERA": "Téracristal",
        "Tag.ANCIENT": "Temps passé",
        "Tag.FUTURE": "Temps futur",
        "PokemonStage.BASIC": "Base",
        "PokemonStage.STAGE1": "Niveau 1",
        "PokemonStage.STAGE2": "Niveau 2",
        "Color.GRASS": "Plante",
        "Color.FIRE": "Feu",
        "Color.WATER": "Eau",
        "Color.LIGHTNING": "Électrique",
        "Color.PSYCHIC": "Psy",
        "Color.FIGHTING": "Combat",
        "Color.DARKNESS": "Obscurité",
        "Color.METAL": "Métal",
        "Color.FAIRY": "Fée",
        "Color.DRAGON": "Dragon",
        "Color.COLORLESS": "Incolore",
        # Misc
        "TextKind.EFFECT": "Effet",
        "TextKind.ABILITY": "Talent",
        "TextKind.ATTACK": "Attaque",
        "evolves_from": "Évolution de : {0}",
        "evolves_into": "Évolue en : {0}",
        "artist_credit": "Illus. {0}",
        "hp": "PV",
        "weakness": "Faiblesse",
        "resistance": "Résistance",
        "retreat": "Retraite",
        ###
        "sv_tera_marker": "Téracristal",
        "sv_tera_text": (
            "Tant que ce Pokémon est sur votre Banc, évitez tous les dégâts infligés à ce Pokémon "
            "par des attaques (les vôtres et celles de votre adversaire)."
        ),
        "sv_ex_rule_title": "Règle des Pokémon-ex",
        "sv_ex_rule_text": (
            "Lorsque votre Pokémon-ex "
            "est mis K.O., l'adversaire récupère 2 cartes Récompense."
        ),
        "sv_supporter_text": (
            "Vous ne pouvez jouer qu'une seule carte " "Supporter pendant votre tour."
        ),
        "sv_stadium_text": (
            "Vous ne pouvez jouer qu'une carte Stade pendant votre tour. "
            "Placez-la à côté du Poste Actif et défaussez-la si un autre Stade "
            "entre en jeu. Un Stade de même nom ne peut pas être joué."
        ),
        "sv_item_text": (
            "Vous pouvez jouer autant de cartes Objet "
            "que vous le voulez pendant votre tour."
        ),
        "sv_tool_text": (
            "Vous pouvez attacher un Outil Pokémon à autant de Pokémon "
            "que vous le voulez pendant votre tour. Vous ne pouvez attacher "
            "qu'un Outil Pokémon à chaque Pokémon, et il reste attaché."
        ),
    },
    "it-IT": {
        "CardType.POKEMON": "Pokémon",
        "CardType.TRAINER": "Allenatore",
        "CardType.ENERGY": "Energia",
        "PokemonStage.BASIC": "Base",
        "PokemonStage.STAGE1": "Fase 1",
        "PokemonStage.STAGE2": "Fase 2",
        "artist_credit": "Ill. {0}",
        "evolves_from": "Si evolve da {0}",
        "evolves_into": "Si evolve in {0}",
        "sv_tera_marker": "Teracristal",
        "sv_tera_text": (
            "Fintanto che questo Pokémon è nella tua panchina, previeni tutti i danni "
            "inflitti a questo Pokémon da qualsiasi attacco, sia tuo che del tuo avversario."
        ),
        "sv_ex_rule_title": "Regola del Pokémon-ex",
        "sv_ex_rule_text": (
            "Il tuo avversario prende due "
            "carte Premio ogni volta che il tuo Pokémon-ex viene messo KO."
        ),
        "sv_supporter_text": (
            "Puoi giocare una sola carta Aiuto durante " "il tuo turno."
        ),
        "sv_stadium_text": (
            "Puoi giocare una sola carta Stadio durante il tuo turno. Mettila "
            "accanto alla posizione attiva e scartala se un'altra carta Stadio entra "
            "in gioco. Non si può giocare una carta Stadio con lo stesso nome."
        ),
        "sv_item_text": (
            "Puoi giocare tutte le carte Strumento che " "vuoi durante il tuo turno."
        ),
        "sv_tool_text": (
            "Puoi assegnare un numero qualsiasi di carte Oggetto Pokémon ai tuoi "
            "Pokémon durante il tuo turno. Puoi assegnare a ciascun Pokémon "
            "un solo Oggetto Pokémon, ed esso rimane assegnato."
        ),
    },
    # Proofread by GC|Linkinboss
    "es-ES": {
        "CardType.POKEMON": "Pokémon",
        "CardType.TRAINER": "Entrenador",
        "CardType.ENERGY": "Energía",
        "PokemonStage.BASIC": "Básico",
        "PokemonStage.STAGE1": "Fase 1",
        "PokemonStage.STAGE2": "Fase 2",
        "artist_credit": "Ilus. {0}",
        "evolves_from": "Evoluciona de {0}",
        "evolves_into": "Evoluciona a {0}",
        "sv_tera_marker": "Teracristal",
        "sv_tera_text": (
            "Mientras este Pokémon esté en tu Banca, se evita todo el daño infligido "
            "a este Pokémon por ataques (tanto tuyos como de tu rival)."
        ),
        "sv_ex_rule_title": "Regla para los Pokémon ex",
        "sv_ex_rule_text": (
            "Cuando tu Pokémon ex "
            "queda Fuera de Combate, tu rival coge 2 cartas de Premio."
        ),
        "sv_supporter_text": (
            "Solo puedes jugar una carta de Partidario " "durante tu turno."
        ),
        "sv_stadium_text": (
            "Solo puedes jugar una carta de Estadio durante tu turno. Colócala "
            "al lado del Puesto Activo y descártala si entra en juego otro Estadio "
            "No se puede jugar un Estadio con el mismo nombre."
        ),
        "sv_item_text": (
            "Puedes jugar cualquier cantidad de cartas de " "Objeto durante tu turno."
        ),
        "sv_tool_text": (
            "Puedes unir cualquier cantidad de Herramientas Pokémon a tus "
            "Pokémon durante tu turno. Solo puedes unir una Herramienta "
            "Pokémon a cada Pokémon, y esta permancerá unida."
        ),
    },
    # Proofread by wiki
    "pt-BR": {
        "CardType.POKEMON": "Pokémon",
        "CardType.TRAINER": "Treinador",
        "CardType.ENERGY": "Energia",
        "PokemonStage.BASIC": "Básico",
        "PokemonStage.STAGE1": "Estágio 1",
        "PokemonStage.STAGE2": "Estágio 2",
        "artist_credit": "Ilust. {0}",
        "evolves_from": "Evolui de {0}",
        "evolves_into": "Evolui para {0}",
        "sv_tera_marker": "Tera",
        "sv_tera_text": (
            "Enquanto este Pokémon estiver no seu Banco, previna todo o dano causado a este "
            "Pokémon por ataques (seus e do seu oponente)."
        ),
        "sv_ex_rule_title": "Regra dos Pokémon ex",
        "sv_ex_rule_text": (
            "Quando seu Pokémon ex "
            "é Nocauteado, seu oponente pega 2 cartas de Prêmio."
        ),
        "sv_supporter_text": (
            "Você só pode jogar 1 carta de Apoiador " "durante o seu turno."
        ),
        "sv_stadium_text": (
            "Você só pode jogar 1 carta de Estádio durante o seu turno. Coloque-a "
            "ao lado do Campo Ativo e descarte-a se outro Estádio entrar em jogo. "
            "Um Estádio com o mesmo nome não pode ser jogado."
        ),
        "sv_item_text": (
            "Você pode jogar quantas cartas de Item " "quiser durante o seu turno."
        ),
        "sv_tool_text": (
            "Você pode ligar quantas Ferramentas Pokémon quiser aos seus  "
            "Pokémon durante o seu turno. Você só pode ligar 1 Ferramenta "
            "Pokémon a cada Pokémon, e ela permanecerá ligada."
        ),
    },
    "ja-JP": {
        "CardType.POKEMON": "ポケモン",
        "CardType.TRAINER": "トレーナーズ",
        "CardType.ENERGY": "エネルギー",
        "PokemonStage.BASIC": "たね",
        "PokemonStage.STAGE1": "1進化",
        "PokemonStage.STAGE2": "2進化",
        "artist_credit": "Illus. {0}",
        "evolves_from": "{0}がら進化",
        "sv_tera_marker": "テラスタル",
        "sv_tera_text": "このポケモンは、ベンチにいるかぎり、ワザのダメージを受けない。",
        "sv_ex_rule_title": "",
        "sv_ex_rule_text": "ポケモンexがきぜつしたとき、相手はサイドを2枚とる。",
    },
    "ru-RU": {
        "artist_credit": "Илл. {0}",
        "evolves_from": "Эволюционирует из {0}",
    },
}


RULES = {
    "sv_tera": {
        "kind": enums.TextKind.EFFECT,
        "name": "sv_tera_marker",
        "text": "sv_tera_text",
        "_sort": 0,
    },
    "sv_ex_rule": {
        "kind": enums.TextKind.RULE_BOX,
        "name": "sv_ex_rule_title",
        "text": "sv_ex_rule_text",
        "_sort": 5,
    },
    "swsh_v_rule": {
        "kind": enums.TextKind.RULE_BOX,
        "name": "swsh_v_rule_title",
        "text": "swsh_v_rule_text",
        "_sort": 5,
    },
    "swsh_vmax_rule": {
        "kind": enums.TextKind.RULE_BOX,
        "name": "swsh_vmax_rule_title",
        "text": "swsh_vmax_rule_text",
        "_sort": 5,
    },
    "sv_supporter": {
        "kind": enums.TextKind.REMINDER,
        "text": "sv_supporter_text",
        "_sort": 5,
    },
    "sv_stadium": {
        "kind": enums.TextKind.REMINDER,
        "text": "sv_stadium_text",
        "_sort": 5,
    },
    "sv_item": {
        "kind": enums.TextKind.REMINDER,
        "text": "sv_item_text",
        "_sort": 5,
    },
    "sv_tool": {
        "kind": enums.TextKind.REMINDER,
        "text": "sv_tool_text",
        "_sort": 5,
    },
}


# artist_credit_forms = [
#     "Illus. {artist}",
#     "Illus. & Direc. {artist}",
#     "Illus. {artist}/Direc. {artist}",
#     "Photo. {artist}",  # Wizards BSP! Pokemon Snap Pikachu
# ]


def _str(locale, key, *args):
    if key:
        return tl_strings[locale][key].format(*args)
    return None


def _rule(locale, which):
    ret = dict(RULES[which])
    if "name" in ret:
        ret["name"] = _str(locale, ret["name"])
    ret["text"] = _str(locale, ret["text"])
    return ret
