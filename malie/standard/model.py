#!/usr/bin/env python3

import sys
from typing import (
    List,
    Literal,
    Optional,
    Union,
)

import pydantic

from . import enums


#
# (´〜｀*) zzz
#


class Artists(pydantic.BaseModel):
    """
    Information regarding the Artist(s) of this card.
    """

    text: str
    list: List[str]


class CollectorNumber(pydantic.BaseModel):
    """
    Collector Number
    """

    full: str
    numerator: str
    denominator: Optional[str]
    numeric: int


class Rarity(pydantic.BaseModel):
    """
    Rarity information for this card
    """

    designation: enums.RarityDesignation
    icon: enums.RarityIcon


class Copyright(pydantic.BaseModel):
    """
    Copyright information
    """

    text: str
    year: int


class Foil(pydantic.BaseModel):
    """
    Foil/holography information
    """

    type: enums.FoilType
    mask: enums.FoilMask


class Weakness(pydantic.BaseModel):
    types: List[enums.Color]
    amount: int
    operator: enums.WeaknessOperator


class Resistance(pydantic.BaseModel):
    types: List[enums.Color]
    amount: int
    operator: enums.ResistanceOperator


class Damage(pydantic.BaseModel):
    amount: int
    suffix: Optional[enums.DamageSuffix]


class TextBox(pydantic.BaseModel):
    kind: enums.TextKind
    name: Optional[str]
    text: Optional[str]
    cost: Optional[List[enums.Color]]
    damage: Optional[Damage]


class Card(pydantic.BaseModel):
    card_type: enums.CardType
    name: str
    # Only used on Trainer cards, but it "feels" essential enough that
    # it isn't worth splitting it out.
    subtitle: Optional[str]
    lang: str
    # Not set on (modern) Energy, but was once upon a time. generally
    # required but it IS absent sometimes.
    artists: Optional[Artists]
    # Absent on basic energy (and earlier cards.) should be set on
    # everything else modern.
    regulation_mark: Optional[str]
    set_icon: str
    collector_number: Optional[CollectorNumber]
    rarity: Optional[Rarity]  # Generally set on everything except setless basic energy.
    copyright: Copyright
    # Tags could theoretically apply to anything; for now however the
    # tags we have apply only to pokemon and trainers
    tags: List[enums.Tag] = []
    size: enums.CardSize
    back: enums.CardBack
    foil: Optional[Foil]
    # Some cards do not have any text at all (Basic Energy) but everything else should.
    text: List[TextBox] = []


class EnergyCard(Card):
    card_type: Literal[enums.CardType.ENERGY]
    subtype: Union[Literal[enums.CardSubType.BASIC, enums.CardSubType.SPECIAL]]
    #
    types: List[
        enums.Color
    ] = []  # only for Basic Energy (field shared with Pokemon type)


class TrainerCard(Card):
    card_type: Literal[enums.CardType.TRAINER]
    subtype: Union[
        Literal[
            enums.CardSubType.ITEM,
            enums.CardSubType.STADIUM,
            enums.CardSubType.TOOL,
            enums.CardSubType.SUPPORTER,
        ]
    ]
    # Very rarely, for PLAYABLE_TRAINER cards (field shared with Pokemon type)
    hp: Optional[int]


class PokemonCard(Card):
    card_type: Literal[enums.CardType.POKEMON]
    #
    stage: enums.PokemonStage
    stage_text: Optional[str]
    hp: int  # field shared with TrainerCard (but is mandatory here)
    types: List[enums.Color]  # field shared with basic energy (but is mandatory here)
    weakness: Optional[Weakness]
    resistance: Optional[Resistance]
    retreat: int
    flavor_text: Optional[str]


class PyCard(pydantic.BaseModel):
    __root__: Union[EnergyCard, TrainerCard, PokemonCard] = pydantic.Field(
        ..., discriminator="card_type"
    )


class ExportDocument(pydantic.BaseModel):
    __root__: List[PyCard]


if __name__ == "__main__":
    if len(sys.argv) > 1:
        print(f"validate '{sys.argv[1]}' ... ", end="")
        ExportDocument.parse_file(sys.argv[1])
        print("OK")
    else:
        print(ExportDocument.schema_json(indent=2))
